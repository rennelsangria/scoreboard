//
//  ViewController.h
//  Scoreboard
//
//  Created by Rennel Sangria on 6/29/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *Player1Score;
@property (weak, nonatomic) IBOutlet UILabel *Player2Score;
@property (weak, nonatomic) IBOutlet UITextField *p1Text;
@property (weak, nonatomic) IBOutlet UITextField *p2Text;
@property (weak, nonatomic) IBOutlet UIStepper *p1Stepper;
- (IBAction)p1StepperValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *p2Stepper;
- (IBAction)p2StepperValueChanged:(id)sender;

-(IBAction)backgroundTouched:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
- (IBAction)resetButtonTouched:(id)sender;


@end

