//
//  ViewController.m
//  Scoreboard
//
//  Created by Rennel Sangria on 6/29/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize Player1Score = _Player1Score;
@synthesize Player2Score = _Player2Score;
@synthesize p1Text = _p1Text;
@synthesize p2Text = _p2Text;
@synthesize p1Stepper =_p1Stepper;
@synthesize p2Stepper = _p2Stepper;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}




- (IBAction)p2StepperValueChanged:(UIStepper *)sender {
    self.Player2Score.text = [NSString stringWithFormat:@"%d",
                              [[NSNumber numberWithDouble:[(UIStepper *) sender value]] intValue]];
    
   // NSLog(@"The stepper 2 has %f value", sender.value);
}



//keyboard stuff
- (IBAction)backgroundTouched:(id)sender{
    [_p1Text resignFirstResponder];
    [_p2Text resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _p1Text){
        [_p1Text resignFirstResponder];
    }
    if (textField == _p2Text){
        [_p2Text resignFirstResponder];
    }
    return NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)p1StepperValueChanged:(id)sender {
    self.Player1Score.text = [NSString stringWithFormat:@"%d",
                              [[NSNumber numberWithDouble:[(UIStepper *) sender value]] intValue]];
}
- (IBAction)resetButtonTouched:(id)sender {
    self.Player1Score.text = @"0";
    self.Player2Score.text = @"0";;
}
@end
